#!/usr/bin/python3

import argparse
import sys
import urllib.error
import urllib.request

def main():
    parser = argparse.ArgumentParser(
        usage='%(prog)s --url URL --token TOKEN --machine-key FILE --outupt FILE')
    parser.add_argument('--url', type=str, metavar='URL', help='URL of the voicelab recognize server')
    parser.add_argument('--token', type=str, metavar='TOKEN', help='authorization token')
    parser.add_argument('--audio-file', type=str, metavar='FILE', help='audio file')
    parser.add_argument('--conf-name', type=str, metavar='CONF', help='configuration name')
    parser.add_argument('--pid', type=str, metavar='PID', help='pid')
    parser.add_argument('--content-type', type=str, metavar='TYPE', help='pid')
    parser.add_argument('--timestamps', action='store_true', default=False)
    args = parser.parse_args()
    if args.url is None:
        sys.exit('error: option --url is required')
    if args.token is None:
        sys.exit('error: option --token is required')
    if args.audio_file is None:
        sys.exit('error: option --audio-file is required')
    if args.conf_name is None:
        sys.exit('error: option --conf-name is required')
    if args.pid is None:
        sys.exit('error: option --pid is required')
    if args.content_type is None:
        sys.exit('error: option --content-type is required')
    if args.timestamps:
        args.timestamps = 'timestamps'

    with open(args.audio_file, 'rb') as f:
        audio_file = f.read()
    del f

    headers = {
		'X-Voicelab-Password': args.token,
		'X-Voicelab-Pid': args.pid,
		'X-Voicelab-Conf-Name': args.conf_name,
		'X-Voicelab-Options': args.timestamps,
		'Content-type': args.content_type,
	}
    req = urllib.request.Request(url=args.url, data=audio_file, headers=headers)
    try:
        resp = urllib.request.urlopen(req)
    except urllib.error.HTTPError as e:
        sys.exit("error: %s" % e)

    print(resp.read().decode('utf-8'))


if __name__ == '__main__':
    main()
